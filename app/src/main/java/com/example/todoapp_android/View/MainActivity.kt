package com.example.todoapp_android.View

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp_android.Adaptors.TaskAdaptor
import com.example.todoapp_android.Models.Task
import com.example.todoapp_android.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity(){

    private lateinit var taskName: EditText

    private lateinit var addToTasksButton: Button

    private lateinit var pendingTasksRV: RecyclerView
    private lateinit var doneTasksRV: RecyclerView

    private lateinit var allTaskArray: ArrayList<Task>
    private lateinit var pendingTaskArray: ArrayList<Task>
    private lateinit var doneTaskArray: ArrayList<Task>

    private lateinit var pendingTaskLabel: TextView
    private lateinit var doneTaskLabel: TextView

    public var taskIdVariable = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Task Bar
        val actionbar = supportActionBar
        actionbar!!.title = "ToDo Application"

        // Add Task Input Field
        taskName = findViewById(R.id.taskDetailsName)

        //Buttons
        addToTasksButton = findViewById(R.id.addButton)

        //Recycler Views
        pendingTasksRV = findViewById(R.id.pendingTasks)
        doneTasksRV  = findViewById(R.id.doneTasks)

        //Task Arrays
        allTaskArray = ArrayList<Task>()
        pendingTaskArray = ArrayList<Task>()
        doneTaskArray = ArrayList<Task>()

        // task Headings
        pendingTaskLabel = findViewById(R.id.pendingTaskLabel)
        doneTaskLabel = findViewById(R.id.doneTaskLabel)


        //Adaptors
        val pendingAdapter = TaskAdaptor(pendingTaskArray, object: TaskAdaptor.TaskAdapterListener{
            override fun doneButtonClick(view: View, position: Int) {
                val task: Task = pendingTaskArray[position]
                task.status = 0
                doneTaskArray.add(task)
                pendingTaskArray.removeAt(position)
                updateData()
                pendingTaskLabel.setText("Pending Tasks (" + pendingTaskArray.size.toString() + "):")
                doneTaskLabel.setText("Done Tasks (" + doneTaskArray.size.toString() + "):")
            }

            override fun cancelButtonClick(view: View, position: Int) {
                pendingTaskArray.removeAt(position)
                updateData()
                pendingTaskLabel.setText("Pending Tasks (" + pendingTaskArray.size.toString() + "):")
            }

            override fun taskRowClick(position: Int) {
                println("In pending Tasks, Row clicked")
                val intent = Intent(this@MainActivity, TaskDetailsActivity::class.java);
                intent.putExtra("task", pendingTaskArray[position])
                startActivity(intent);
            }
        })
        val doneAdaptor = TaskAdaptor(doneTaskArray, object : TaskAdaptor.TaskAdapterListener{
            override fun cancelButtonClick(view: View, position: Int) {
                doneTaskArray.removeAt(position)
                updateData()
                doneTaskLabel.setText("Done Tasks (" + doneTaskArray.size.toString() + "):")
            }

            override fun doneButtonClick(view: View, position: Int) {
                println("Done Button")
            }

            override fun taskRowClick(position: Int) {
                val intent = Intent(this@MainActivity, TaskDetailsActivity::class.java);
                intent.putExtra("task", doneTaskArray[position])
                startActivity(intent);
            }

        })

        addToTasksButton.setOnClickListener(){
            val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
            val currentDate = sdf.format(Date())
            val task: Task = Task(taskName.text.toString(), 1, taskIdVariable++, currentDate)
            if (task.taskName?.length != 0) {
                pendingTaskArray.add(task)
                pendingAdapter.notifyDataSetChanged()
                pendingTaskLabel.setText("Pending Tasks (" + pendingTaskArray.size.toString() + "):")
            }

        }




        pendingTasksRV.adapter = pendingAdapter
        pendingTasksRV.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        doneTasksRV.adapter = doneAdaptor
        doneTasksRV.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

    }

    fun updateData(){
        pendingTasksRV.adapter?.notifyDataSetChanged()
        doneTasksRV.adapter?.notifyDataSetChanged()

    }
}