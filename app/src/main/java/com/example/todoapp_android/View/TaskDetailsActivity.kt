package com.example.todoapp_android.View

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import com.example.todoapp_android.Models.Task
import com.example.todoapp_android.R
import java.text.SimpleDateFormat
import java.util.*

class TaskDetailsActivity : AppCompatActivity() {

    private lateinit var taskDetailsId: TextView
    private lateinit var taskDetailsTitle: TextView
    private lateinit var taskDetailsName: TextView
    private lateinit var taskDetailsCategory: TextView
    private lateinit var taskDetailsCreatedOn: TextView
    private lateinit var backButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_details)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Action Bar
        val actionbar = supportActionBar
        actionbar!!.title = "Task details"

        taskDetailsId = findViewById(R.id.taskDetailsId)
        taskDetailsTitle = findViewById(R.id.taskDetailsTitle)
        taskDetailsCategory = findViewById(R.id.taskDetailsCategory)
        taskDetailsName = findViewById(R.id.taskDetailsName)
        taskDetailsCreatedOn = findViewById(R.id.taskDetailsCreatedOn)

        val task = intent.getSerializableExtra("task") as? Task
        val category = if (task?.status == 1) "Pending" else "Completed"

        taskDetailsName.text = "Task Name: " + task?.taskName
        taskDetailsId.text = "Task Id: " + task?.taskId.toString()
        taskDetailsCategory.text = "Task Status: " + category
        taskDetailsTitle.text = "TASK DETAILS"
        taskDetailsCreatedOn.text = "Created On: " + task?.createdOn

        backButton = findViewById(R.id.backButton)
        backButton.setOnClickListener() {
            onBackPressed()
        }
    }
}