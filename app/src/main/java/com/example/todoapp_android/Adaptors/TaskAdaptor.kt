package com.example.todoapp_android.Adaptors
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp_android.Models.Task
import com.example.todoapp_android.R

class TaskAdaptor(private val tasks: ArrayList<Task>, private val taskAdapterListener: TaskAdapterListener) : RecyclerView.Adapter<TaskAdaptor.ItemViewHolder>(){

    class ItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val taskRow: TextView = itemView.findViewById(R.id.taskRow)
        val doneButton: Button = itemView.findViewById(R.id.doneButton)
        val cancelButton = itemView.findViewById<Button>(R.id.deleteButton)


        fun setData(name: String, visibility: Int){
            taskRow.text = name
            if (visibility == 0)
                doneButton.visibility = Button.GONE
        }

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.task_row_item, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        tasks[position].taskName?.let { holder.setData(it, tasks[position].status) }

        holder.doneButton.setOnClickListener(){
            taskAdapterListener.doneButtonClick(it, position)
        }
        holder.cancelButton.setOnClickListener(){
            taskAdapterListener.cancelButtonClick(it, position)
        }
        holder.taskRow.setOnClickListener(){
            taskAdapterListener.taskRowClick(position)
        }


    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    public interface TaskAdapterListener{
        fun doneButtonClick(view: View, position: Int)
        fun cancelButtonClick(view: View, position: Int)
        fun taskRowClick(position: Int)
    }


}