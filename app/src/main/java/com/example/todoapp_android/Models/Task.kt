package com.example.todoapp_android.Models

class Task : java.io.Serializable {
    var taskName: String? = null
    var status: Int = 0
    var taskId: Int = 0
    var createdOn: String? = null

    constructor(taskName: String?, status: Int, taskId: Int, createdOn: String) {
        this.taskName = taskName
        this.status = status
        this.taskId = taskId
        this.createdOn = createdOn
    }
}